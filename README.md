#### Jenkins Multi Branch Pipeline
</details>


***
This is a multi-branch jenkins pipeline repository


The focus of this repository is to demonstrate three (3) things
Previous pipeline setups demonstrated CI but this focuses on CD.


This repository still reflects the complete process of CI/CD
See the master branch, within the jenkins file the deploy stage demonstrates this.


1. Setting up a multi-branch that can be used in a jenkins multi-branch pipeline.
2. Complete the CI/CD pipeline setup where jenkins deploys the application (docker image) to an EC2 instance.
3. Multi-branch pipeline credentials scope


The application can be reached here. It corresponds to the setup within the deploy stage in the jenkinsfile within the master branch: http://54.194.16.113:3000/


<details>




**<summary>Setup Steps</summary>**
<br />
Within the multi-branch pipeline configuration, the ec2-user server key will be stored here as a credentials that will be used to connect to the respective ec2 instance.




</details>


******
